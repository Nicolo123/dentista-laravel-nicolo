<x-layout>
    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6 shadow form-h">

                
                <form action="{{Route('contact.submit')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label">Nome e Cognome</label>
                        <input name="name" type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp">
                    </div> 
                    <div class="mb-3">
                        <label for="exampleInputEmail" class="form-label">Inserire Email</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail">
                    </div>
                    <div class="mb-3 d-flex justify-content-center my-5">
                        <label for="exampleInputMessagge" class="form-label">Il tuo Messaggio</label>
                    </div>
                    <div class="d-flex justify-content-center">
                        <textarea name="messagge" id="exampleInputMessagge" cols="30" rows="10"></textarea>
                    </div>    
                    <div class="my-1 d-flex justify-content-center align-items-center my-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>










</x-layout>