<x-layout>
<header class="header">
    @if (session('flash'))
    <div class="alert alert-success">
        {{ session('flash') }}
    </div>
    @endif
</header>
<section>
    <div class="container-fluid go-to-back">
        <div class="row justify-content-around align-items-center h-100">
            <div class="col-12 col-md-6 card-header">
                <h2 class="text-center card-title">I nostri servizi</h2>
                <p class="card-p">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora aut in nobis ipsam qui quibusdam, magni est sit magnam harum, ut cum vel quisquam rerum doloremque, eveniet similique obcaecati blanditiis!</p>
                <div class="d-flex justify-content-center align-items-center div-butt">
                    <button class="btn btn-custom">
                        <a class="dent-a" href="">Servizi</a>
                    </button>
                </div>
            </div>
            <div class="col-12 col-md-6 card-header">
                <h2 class="text-center card-title">Contattaci</h2>
                <p class="card-p">Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse aliquam possimus voluptates quam tenetur maxime ullam recusandae iste corrupti dolorem cumque, earum illum? Voluptates, doloribus natus rerum eligendi eaque cupiditate!</p>
                <div class="d-flex justify-content-center align-items-center div-butt">
                    <button class="btn btn-custom">
                        <a class="dent-a" href="">contatti</a>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid margin-custom">
        <div class="row">
            <div class="col-4">
                <img class="img-fluid dent-img" src="./media/dentist.png" alt="">
            </div>
            <div class="col-8 d-flex flex-column">
                <div class="my-3 div-dent">
                    <h3 class="dent-title">Passione</h3>
                    <p class="dent-p">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Facere voluptas exercitationem tempora placeat eos, eius rem reprehenderit saepe veritatis itaque laboriosam, non beatae accusamus odit magnam cumque, repudiandae nam magni?</p>
                </div>
                <div class="my-3 div-dent">
                    <h3 class="dent-title">Disponibilità</h3>
                    <p class="dent-p">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem, sequi. Excepturi iusto dolores dolorum, ipsum fuga beatae pariatur consequatur dolorem voluptates quibusdam molestias mollitia eum asperiores praesentium minus dolore eius?</p>
                </div>
                <div class="my-3 div-dent">
                   <h3 class="dent-title">Serietà</h3>
                   <p class="dent-p">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo officiis consequuntur odio ratione placeat. Maxime unde, nesciunt ratione dolor veniam labore, rerum repellendus delectus consequuntur temporibus quasi dolores porro quia!</p>
                </div>
                <div class="d-flex justify-content-center my-3">
                    <button class="btn btn-custom">
                        <a class="dent-a" href=""> Chi siamo</a>
                    </button>
                </div>

            </div>
        </div>
    </div>
</section>







    
  

</x-layout>