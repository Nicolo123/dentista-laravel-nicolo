<x-layout>
<section class="dettaglio">
    
    <div class="container-fluid">
        <div class="row ">
            <div class="col 12 margin-custom">
                <h2 class="text-center dent-title">{{$service['name']}}</h2>
                <div class="d-flex justify-content-center">
                    <img class="shadow my-4" src="{{$service['photo']}}" alt="">
                </div>
                <p>{{$service['text']}}</p>
            </div>
        </div>
    </div>
</section>















</x-layout>