<x-layout>
    <section>
        
    <div class="container-fluid">
        <div class="row justify-content-center">
            
            @foreach ($dentists as $dentist)
                <div class="col-12 col-md-6 margin-custom d-flex justify-content-center">
                    <div class="card card-dent w-50" style="width: 18rem;">
                        <img src="{{$dentist['photo']}}" class="card-img-top" alt="foto dentista">
                        <div class="card-body">
                          <h5 class="card-title dent-title text-center dentist-title">{{$dentist['name']}}</h5>
                          <p class="card-text text-dent my-4 text-center">{{$dentist['role']}}</p>
                         
                        </div>
                      </div>
            </div>
            @endforeach
        </div>
    </div>
</section>















</x-layout>