<x-layout>
    <section>
        
        <div class="container-fluid">
            <div class="row justify-content-center">
                
                @foreach ($services as $service)
                    <div class="col-12 col-md-6 margin-custom d-flex justify-content-center">
                        <div class="card card-dent w-50" style="width: 18rem;">
                            <img src="{{$service['photo']}}" class="card-img-top" alt="foto dentista">
                            <div class="card-body">
                                <p class="card-text d-none">{{$service['id']}}</p>
                              <h5 class="card-title dent-title text-center dentist-title">{{$service['name']}}</h5>
                              <p class="card-text text-dent my-4 text-center">{{$service['details']}}</p>
                              <div class="d-flex justify-content-center">

                                  <button class="btn btn-service">
                                    <a class="text-decoration-none text-black" href="{{Route('dettaglio', ['id' => $service['id']])}}">
                                        Scopri di più
                                    </a>
                                    
                                </button>
                              </div>
                            </div>
                          </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>



























</x-layout>