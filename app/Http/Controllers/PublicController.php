<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homepage(){
        return view('homepage');
    }
    public function chiSiamo(){
        $dentists = [
            ['id' => 1, 'name' => 'Michele Foliero', 'exp' => 30, 'role' => 'Ciao, sono Michele Foliero, odontoiatra specializzato in chirurgia odontostomatologica. Sai quante volte ho visto persone spensierate, simpatiche e piene di vita, limitare se non nascondere il proprio sorriso? Io le aiuto a sorridere con orgoglio, senza vergogna e senza imbarazzo. A volte basta davvero poco….a volte il lavoro da fare è un po’ più impegnativo. Ma il risultato finale è sempre una soddisfazione! Credo che il dovere di un medico sia di ascoltare i pazienti e di farli sentire sempre a proprio agio, solo così affronteranno bene qualsiasi tipologia di intervento. Nel mio studio questo è un mantra….e oltre ad ascoltarti e trattarti con tanta gentilezza, cerchiamo anche di farti stare in un ambiente confortevole e rilassante.', 'photo' => "media/michelefoliero.jpg"],
            ['id' => 2, 'name' => 'Mirella Mazzanti', 'exp' => 10, 'role' => 'Mi sono laureata nel 2008 con lode e ho iniziato subito dopo la libera professione come odontoiatra presso studi medici privati. Attualmente mi occupo prevalentemente di conservativa ed endodonzia. Con gli anni e la pratica clinica infatti, ho raggiunto la consapevolezza di prediligere un atteggiamento conservativo degli elementi naturali in quanto più congeniale alla mia indole e alla mia filosofia di limitare l’invasività degli interventi.', 'photo' => '/media/mirellamazzanti.jpg'],
            ['id' => 3, 'name' => 'Lucrezia Manfrin', 'exp' => 8, 'role' => 'Gentile, simpatica e molto professionale. Senza di lei saremmo persi! È la persona che ci prepara la postazione di lavoro, i materiali e gli strumenti necessari per svolgere gli interventi, come l’installazione di protesi e impianti dentali, cura delle carie, devitalizzazioni, interventi di chirurgia dentale. Durante le sedute con i pazienti, è lei che passa gli strumenti al dentista e fornisce assistenza continua. Con Rosy si lavora nella più completa sintonia e questo è importantissimo per noi. Quando verrai a trovarci ti accorgerai di quanto è brava a metterti a tuo agio!', 'photo' => '/media/lucreziamanfrin.jpg'],
            ['id' => 4, 'name' => 'Eliana Lettiere', 'exp' => 2, 'role' => 'fresca di diploma da odontotecnico , la sua dolce voce vi risponderà al telefono e sarà il primo sorriso a introdurvi nel nostro studio. Simpatica e dinamica , si sta preparando , come ASO , per diventare un pilastro della nostra giovane equipe.','photo' => '/media/elianalettiere.jpg']
        ];

            return view('chi-siamo',['dentists' => $dentists]);
    }
    public function servizi(){
        $services = [
            ['id' => 1, 'name' => 'igiene dentale', 'details' => 'I nostri servizi di igiene', 'photo' =>'/media/filo.jpg'],
            ['id' => 2, 'name' => 'ortodonzia', 'details' => 'I nostri servizi di odontoiatria', 'photo' =>'/media/Ortodonzia.jpg'],
            ['id' => 3, 'name' => 'chirurgia', 'details' => 'I nostri servizi di chirurgia', 'photo' =>'/media/chirurgia.jpg'],
            
        ];
        return view('servizi', ['services' => $services]);
    }

    public function dettaglio($id){
        $services = [
            ['id' => 1, 'name' => 'igiene dentale', 'details' => 'I nostri servizi di igiene', 'photo' =>'/media/filo.jpg', 'text' => 'Nel nostro studio diamo molta importanza all’educazione ed alla prevenzione dentale, non solo per prevenire l’insorgenza delle diverse patologie orali ma anche per mantenere nel tempo la situazione di stabilità raggiunta dopo una terapia odontoiatrica. Per questo, al completamento delle cure viene stabilito un programma di richiami di igiene, profilassi e mantenimento ad intervallo variabile (da 2 a 10 mesi) in relazione al rischio futuro di malattia ed alle personali performances in igiene orale. Una volta all’anno verranno inoltre eseguite nell’ambito della seduta di igiene delle radiografie digitali di contro'],
            ['id' => 2, 'name' => 'ortodonzia', 'details' => 'I nostri servizi di odontoiatria', 'photo' =>'/media/Ortodonzia.jpg', 'text' => 'L’ortodonzia é il settore dell’Odontoiatria che studia e corregge le anomalie che possono interessare la posizione dei denti, la crescita delle ossa mascellari (osso mascellare superiore e osso mandibolare) e i rapporti tra le due arcate dentarie (malocclusioni). Tali anomalie possono dipendere da fattori ereditari, dalla perdita prematura di denti da latte o dall’estrazione precoce di denti permanenti. L’Ortodonzia ha quindi lo scopo di ripristinare il corretto rapporto fra le arcate, l’armonia facciale, la masticazione, la fonesi e la respirazione.Le malocclusioni sono prodotte da anomalie della posizione e dello sviluppo dei denti (disallineamento dentale) e da alterazioni costituzionali delle ossa mascellari e mandibolari. L’ortodonzia quindi si occupa di trattare e correggere tali anomalie che possono avere ripercussioni importanti sia a livello funzionale che estetico, inducendo disturbi a carico della muscolatura masticatoria e delle articolazioni temporo-mandibolari. Le malocclusioni dentali provocano inoltre significative alterazioni dell’equilibrio fra struttura dentale, strutture ossee, muscolari e tendinee, ricadendo negativamente sulla colonna vertebrale, provocando cefalee, squilibri posturali, algie cervicali e vertebrali, disturbi della vista e dell’udito. L’ortodonzia moderna non si limita dunque al corretto allineamento dei denti, ma promuove il raggiungimento di un equilibrio ottimale fra efficienza della masticazione, salute dentale, estetica del sorriso e benessere complessivo del paziente'],
            ['id' => 3, 'name' => 'chirurgia', 'details' => 'I nostri servizi di chirurgia', 'photo' =>'/media/chirurgia.jpg', 'text' => 'Oggi le persone che si recano dal dentista non richiedono soltanto denti sani ma anche e sempre di più un miglioramento del proprio aspetto. Un bel sorriso è importante perché ci fa sentire meglio con noi stessi e con gli altri, nel lavoro come nella vita privata. I nostri servizi dello Studio dott. Ciro Eccher per l’estetica dentale:'],
            ];
            foreach($services as $service){
                if($service['id'] == $id){
                    return view('dettaglio-servizio', ['service' => $service]);
                }
            }

        
    }

    public function form(){
        return view('form');
    }
    public function contactUS(){
        return view('contattaci');
    }

    public function contactSubmit(Request $request){
        $user = $request->input('name');
        $email = $request->input('email');
        $messagge = $request->input('messagge');


        $user_contact = compact('user', 'email', 'messagge');
        Mail::to($email)->send(new ContactMail($user_contact));

        return redirect(route('homepage'))->with('flash' ,'La tua email è stata correttamente inviata');
    }
}
