<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PublicController::class, 'homepage'])->name('homepage');
Route::get('chi-siamo',[PublicController::class, 'chiSiamo'])->name('chisiamo');
Route::get('servizi',[PublicController::class, 'servizi'])->name('servizi');
Route::get('dettaglio/{id}',[PublicController::class, 'dettaglio'])->name('dettaglio');
Route::get('form',[PublicController::class, 'form'])->name('form');
Route::post('form/submit', [PublicController::class, 'contactSubmit'])->name('contact.submit');